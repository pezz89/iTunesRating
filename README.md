# iTunes Rating App
This app was developed in response to this reddit post: https://www.reddit.com/r/programmingrequests/comments/2xipga/request_rating_app_for_itunes_with_flashing_window/

I started this just as an experiment to see what creating a front end to a python app was like and also to see how applescript worked on mac.
This project is no longer under development as the client stopped responding and as a result I lost interest. 
However the app still works properly and uses wxPython and a python applescript bridge to communicate with itunes.
It has been a while since I used this project but I believe it was packaged with py2app. I can work this out again on request.
It did also run on the command line by running the iTunes Rating App.py script 
